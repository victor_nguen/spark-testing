package com.tsc

import com.tsc.common.sparktest.TestResult

package object exceptions {
  sealed abstract class SparkTestException(m: String) extends Exception(m)

  class InvalidFileNameException(file: String)
      extends SparkTestException(s"Cannot parse file: $file")

  class TestParsingException(m: String) extends SparkTestException(m)

  class WrongTestPairException(source: String, target: String)
      extends SparkTestException(s"$source and $target are not compatible pair of tests")

  class UnknownLoggerException(message: String) extends SparkTestException(message)

  sealed abstract class AirflowInformativeException(message: String) extends Exception(message)

  class AirflowTestInfoException(message: String) extends AirflowInformativeException(message)

  class AnalysisSimpleException(simpleMessage: String)
      extends AirflowInformativeException(simpleMessage)

  class SucceedTestInfoException(testResult: TestResult)
      extends AirflowInformativeException(
        s"""${testResult.test.sourcePath.getParent.getFileName}/Test #${testResult.test.number} type: ${testResult.test.testType} - SUCCEED"""
      )

  class FailedTestInfoException(testResult: TestResult)
      extends AirflowInformativeException(
        s"""${testResult.test.sourcePath.getParent.getFileName}/Test #${testResult.test.number} type: ${testResult.test.testType} - FAILED
           ${testResult.exception match {
            case Some(exception) => s"|Thrown exception: $exception"
            case None            => "|"
          }}
           ${testResult.logger.getMessage match {
            case Some(value) => s"|Message: $value"
            case None        => "|"
          }}
           |""".stripMargin.trim
      )
}
