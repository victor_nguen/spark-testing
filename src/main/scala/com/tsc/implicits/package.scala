package com.tsc

import com.tsc.common.functions._
import com.tsc.common.logging.loggers.ValidationLogger
import com.tsc.common.sparktest.SparkTest
import com.tsc.common.sparktest.TestExecutionLogic._
import org.apache.spark.sql.{DataFrame, Dataset, SparkSession}

import java.util.Locale

package object implicits {
  implicit class DatasetTestOps[A](self: Dataset[A])(implicit logger: ValidationLogger){
    def compareEqualityTo(that: Dataset[A], selfId: String, thatId: String): Boolean =
      validators.assertEquality(self, that, selfId, thatId)

    def compareEqualityTo(that: Dataset[A], id: String): Boolean =
      validators.assertEquality(self, that, id)

    def compareEqualityTo(that: Dataset[A], selfSortList: List[String], thatSortList: List[String]): Boolean =
      validators.assertEquality(self, that, selfSortList, thatSortList)

    def compareEqualityTo(that: Dataset[A], sortList: List[String]): Boolean =
      validators.assertEquality(self, that, sortList)

    def compareEqualityTo(that: Dataset[A]): Boolean =
      validators.assertEquality(self, that)

    def compareEqualityPreciselyTo(that: Dataset[A]): Boolean =
      validators.assertEqualityPrecisely(self, that)

    def compareDdlTo(that: Dataset[A]): Boolean =
      validators.compareDDl(self, that)

    def compareEqualityByCountTo(that: Dataset[A]): Boolean = validators.assertEqualityByCount(self, that)

    def assertDateFormat(colName: String,
                         dateFormat: String,
                         locale: Locale = Locale.getDefault,
                         maxDeviation: Double = 0.0
    )(implicit spark: SparkSession): Boolean =
      validators.assertDateTimeFormat(self, colName, dateFormat, locale, maxDeviation)

    def assertPattern(colName: String, pattern: String): Boolean =
      validators.assertColumnByRegex(self, colName, pattern)

    def unApply: Dataset[A] = self
  }

  implicit class DatasetOps[A](self: Dataset[A]){
    def isEmpty: Boolean = self.head(1).isEmpty

    def exceptAll(that:Dataset[A]): DataFrame = functions.exceptAll(self, that)
  }

  implicit class StringToPathOps(self: String){
    def toResourcePath: String = getResource(self).getPath
  }

  implicit class SparkTestOps(test: SparkTest) {
    def countTest(implicit
                  spark: SparkSession,
                  logger: ValidationLogger
                 ): Boolean = processCountTest(test)

    def arraysTest(implicit
                   spark: SparkSession,
                   logger: ValidationLogger
                  ): Boolean = processArraysTest(test)

    def ddlTest(implicit spark: SparkSession, logger: ValidationLogger): Boolean =
      processDdlTest(test)

    def constantsTest(implicit
                      spark: SparkSession,
                      logger: ValidationLogger
                     ): Boolean = processConstantsTest(test)
  }
}
