package com.tsc.implicits

import org.apache.spark.sql.{Column, DataFrame, Dataset}

object functions {
  def exceptAll[A](df1: Dataset[A], df2: Dataset[A]): DataFrame = {
    val except = df1.except(df2)

    val columns         = df1.columns
    val colExpr: Column = df1(columns.head) <=> except(columns.head)
    val joinExpr = columns.tail.foldLeft(colExpr) { (colExpr, p) =>
      colExpr && df1(p) <=> except(p)
    }

    val join = df1.join(except, joinExpr, "inner")

    join.select(df1("*"))
  }
}
