package com.tsc.common.logging.factories

import com.tsc.common.logging.loggers.{StdValidationLogger, ValidationLogger}

object StdValidationLoggerFactory extends ValidationLoggerFactory {
  override def getLogger: ValidationLogger = new StdValidationLogger
}
