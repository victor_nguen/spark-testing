package com.tsc.common.logging.factories

import com.tsc.common.logging.loggers.{ValidationLogger, DataDifferenceLogger}

class DataDifferenceLoggerFactory(rowToShowLimit: Int) extends ValidationLoggerFactory {
  override def getLogger: ValidationLogger = DataDifferenceLogger(rowToShowLimit)
}

object DataDifferenceLoggerFactory{
  def apply(rowToShowLimit: Int) = new DataDifferenceLoggerFactory(rowToShowLimit)
}
