package com.tsc.common.logging.factories

import com.tsc.common.logging.loggers.ValidationLogger

trait ValidationLoggerFactory {
  def getLogger:ValidationLogger
}
