package com.tsc.common.logging.loggers

import org.apache.spark.sql.Dataset

trait FileValidationLogger extends ValidationLogger {
  def write[A](left: Dataset[A], right: Dataset[A], leftId: String, rightId: String): Unit
}
