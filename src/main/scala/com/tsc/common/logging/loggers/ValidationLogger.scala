package com.tsc.common.logging.loggers

import org.apache.spark.sql.Dataset

trait ValidationLogger {
  def write[A](left: Dataset[A], right: Dataset[A]): Unit

  def write[A](df: Dataset[A]): Unit

  def info(message: String): Unit = println(message)

  def getMessage: Option[String] = None
}
