package com.tsc.common.logging.loggers

import org.apache.spark.sql.Dataset

class DebugValidationLogger extends ValidationLogger {
  override def write[A](df: Dataset[A]): Unit = df.show(false)

  override def write[A](left: Dataset[A], right: Dataset[A]): Unit = {
    println("EXPECTED TABLE")
    left.show( false)
    println("GIVEN TABLE")
    right.show( false)
  }
}
