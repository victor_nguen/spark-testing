package com.tsc.common.logging.loggers

import org.apache.spark.sql.Dataset

import scala.collection.mutable

class StdValidationLogger extends ValidationLogger {
  private val messageBuilder: mutable.StringBuilder = new mutable.StringBuilder()

  override def write[A ](left: Dataset[A], right: Dataset[A]): Unit = Unit

  override def write[A ](df: Dataset[A]): Unit = Unit

  override def info(message: String): Unit = this.messageBuilder ++= message

  override def getMessage: Option[String] = if (messageBuilder.nonEmpty) Some(messageBuilder.mkString) else None
}
