package com.tsc.common.logging.loggers

import org.apache.spark.sql.functions.col
import org.apache.spark.sql.{Dataset, SaveMode}

/** Logger, that writes Dataframe into file (txt, json, csv, parquet, orc) without
  * repartitioning
  * @param path
  *   path to write
  * @param format
  *   file format (txt, json, csv, parquet, orc)
  * @param options
  *   DataFrameWriter options
  */
//noinspection ScalaCustomHdfsFormat
class BasicFileValidationLogger(path: String,
                                format: String,
                                saveMode: SaveMode,
                                options: Map[String, String] = Map(),
                                var maxRows:Int
) extends FileValidationLogger {
  assert(List("txt", "json", "parquet", "csv", "orc").contains(format),
         "format should be one of: txt, json, csv, parquet, orc"
  )

  override def write[A](left: Dataset[A],
                        right: Dataset[A],
                        leftId: String,
                        rightId: String
  ): Unit = {
    assert(maxRows > 0, "can't write negative quantity of rows")
    val expected = left
      .select(left.columns.map(c => col(c).as(s"EXPECTED_$c")): _*)
    val given = right
      .select(right.columns.map(c => col(c).as(s"GIVEN_$c")): _*)
    val result = expected
      .join(
        given,
        expected.col(s"EXPECTED_$leftId") === given.col(s"GIVEN_$rightId")
      )
      .limit(maxRows)
    result.write
      .format(format)
      .mode(saveMode)
      .options(options)
      .save(path)
  }

  override def write[A ](left: Dataset[A], right: Dataset[A]): Unit = {
    left.write
      .format(format)
      .options(options)
      .save(path)
    right.write
      .format(format)
      .options(options)
      .save(path)
  }

  override def write[A ](df: Dataset[A]): Unit = df.write
    .format(format)
    .options(options)
    .save(path)
}
