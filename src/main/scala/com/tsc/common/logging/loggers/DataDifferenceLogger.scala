package com.tsc.common.logging.loggers

import org.apache.spark.sql.Dataset

import scala.collection.mutable

/**
 * Logging messages and difference of comparable datasets
 * @param rowLimit maximum number of rows that will be logged
 */
class DataDifferenceLogger(rowLimit: Int) extends ValidationLogger {
  private val message: mutable.StringBuilder = new mutable.StringBuilder()
  override def write[A](left: Dataset[A], right: Dataset[A]): Unit =
    message ++= left.limit(rowLimit).collect
      .map(_.toString)
      .mkString("Expected:[\n", ";\n", "]\n") +
      right.limit(rowLimit).collect
      .map(_.toString)
      .mkString("Given:[\n", ";\n", "]\n")

  override def write[A](df: Dataset[A]): Unit =
    message ++= df.limit(rowLimit).collect.mkString("Data: [\n", ";\n", "]\n")


  override def info(message: String): Unit = this.message ++= message

  override def getMessage: Option[String] = message match {
    case m if message.nonEmpty => Some(m.mkString)
    case _ => None
  }
}

object DataDifferenceLogger {
  def apply(rowLimit: Int): DataDifferenceLogger = new DataDifferenceLogger(rowLimit)
}
