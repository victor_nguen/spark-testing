package com.tsc.common.logging.loggers

import org.apache.spark.sql.Dataset

class VoidValidationLogger extends ValidationLogger {
  override def write[A ](df: Dataset[A]): Unit = ()

  override def write[A ](left : Dataset[A],
                     right: Dataset[A]): Unit = ()

  override def info(message: String): Unit = ()
}
