package com.tsc.common

import org.apache.hadoop.conf.Configuration
import org.apache.hadoop.fs.{FSDataInputStream, FileSystem, Path}

import java.io.{BufferedReader, File, InputStreamReader}
import java.net.URI
import java.nio.charset.{Charset, StandardCharsets}
import java.util.jar.JarFile
import scala.io.Source
import scala.collection.JavaConversions._

package object functions {

  /** Indicates whether the execution is from the IDE (false) or from the JAR (true) */
  lazy val inJar: Boolean = new File(
    getClass.getProtectionDomain.getCodeSource.getLocation.getPath
  ).isFile

  /** Read text file from hdfs, local file system or other Hadoop-supported FS
    *
    * @param uri
    *   URI of file, can be neither path in local file system or URL
    * @param encoding
    *   file encoding
    * @return
    *   text read with specified encoding
    */
  def readTextFileHadoop(uri: String, encoding: Charset = StandardCharsets.UTF_8): String = {
    val conf: Configuration   = new Configuration()
    val fs: FileSystem        = FileSystem.get(URI.create(uri), conf)
    var in: FSDataInputStream = null
    var br: BufferedReader    = null
    try {
      in = fs.open(new Path(uri))
      br = new BufferedReader(new InputStreamReader(in, encoding))
      Stream.continually(br.readLine).takeWhile(_ != null).mkString("\n")
    } catch {
      case _: Exception => throw new Exception("read text file failed")
    } finally {
      in.close()
      br.close()
    }
  }

  /** Read text file from resources folder
    * @param path
    *   path to file
    * @param encoding
    *   file encoding, default is UTF-8
    * @return
    *   text read with specified encoding
    */
  def readResource(path: String, encoding: Charset = StandardCharsets.UTF_8): String = {
    Source
      .fromInputStream(
        getClass.getResourceAsStream(path),
        encoding.name
      )
      .mkString
  }

  /** Read file in resources folder and return List of read lines
    * @param path
    *   absolute path to file relational to resources folder
    * @param encoding
    *   file encoding
    */
  def getLinesFromResource(path: String,
                           encoding: Charset = StandardCharsets.UTF_8
  ): List[String] =
    Source
      .fromInputStream(
        getClass.getResourceAsStream(path),
        encoding.name
      )
      .getLines()
      .toList

  /** Read file located in local file-system
    * @param uri
    *   URI to file
    * @param encoding
    *   file encoding, use StandardCharsets to pass this parameter
    * @return
    *   text read with specified encoding
    */
  def readResource(uri: URI, encoding: Charset): String = {
    val src = Source.fromFile(uri, encoding.name)
    try src.mkString
    finally src.close()
  }

  /** Read file located in local file-system with UTF-8
    * @param uri
    *   URI to file
    * @return
    *   text read with UFT-8
    */
  def readResource(uri: URI): String =
    readResource(uri, StandardCharsets.UTF_8)

  /** Making list of files in specified folder in resources folder, working when running inside
    * JAR as well as in IDE
    * @param folder
    *   folder, from which you want to get list of files
    * @return
    *   list of files
    */
  def getResourceFolderFiles(folder: String): Option[List[File]] = {
    val jarFile    = new File(getClass.getProtectionDomain.getCodeSource.getLocation.getPath)
    // executing when running jar
    if (jarFile.isFile) {
      val jar = new JarFile(jarFile)
      val foundedFiles = jar.entries
        .filter(_.getName.startsWith(s"$folder/"))
        .map(e => new File(e.getName))
        .toList
      jar.close()
      foundedFiles match {
        case _ :: _ => Some(foundedFiles)
        case Nil => None
      }
    }
    // executing when running from IDE
    else {
      val file = getResource(folder)
      if (file.isDirectory) Some(file.listFiles.toList)
      else None
    }
  }

  def getResourceFolderFile(path: String): Option[File] = {
    val jarFile = new File(getClass.getProtectionDomain.getCodeSource.getLocation.getPath)
    // executing when running jar
    if (jarFile.isFile) {
      val jar              = new JarFile(jarFile)
      val firstFoundedFile = jar.entries.find(_.getName == path)
      jar.close()
      firstFoundedFile.map(e => new File(e.getName))
    }
    // executing when running from IDE
    else {
      val file = getResource(path)
      if (file.isFile) Some(file)
      else None
    }
  }

  /** Get source from resources folder
    * @param path
    *   path relative to resources folder
    */
  def getResource(path: String): File = {
    val loader = Thread.currentThread().getContextClassLoader
    val url    = loader.getResource(path)
    new File(url.getPath)
  }

}
