package com.tsc.common.sparktest

import com.tsc.common.functions._
import com.tsc.common.logging.loggers.ValidationLogger
import com.tsc.common.sparktest
import com.tsc.exceptions._
import com.tsc.implicits._
import org.apache.spark.sql.{DataFrame, SparkSession}

import java.io.File
import java.nio.charset.{Charset, StandardCharsets}
import java.nio.file.Path
import scala.util.Random

/** Possible types of tests */
object TestTypes extends Enumeration {
  type TestType = Value

  val Count, Arrays, DDL, Constants = Value

  /** Get type from String */
  def fromString(str: String): Option[TestTypes.Value] = Map(
    "count"     -> Count,
    "counts"    -> Count,
    "arrays"    -> Arrays,
    "equality"  -> Arrays,
    "ddl"       -> DDL,
    "constants" -> Constants,
    "const"     -> Constants
  ).get(str.toLowerCase)
}

case class SparkTest private (number: Int,
                              sourcePath: Path,
                              targetPath: Path,
                              sourceExpr: String,
                              targetExpr: String,
                              testType: TestTypes.TestType,
                              encoding: String = "utf-8"
) {

  /** Starts test execution
    * @param spark
    *   SparkSession instance necessary to load data and create Datasets
    * @param logger
    *   ValidationLogger necessary to log difference, which can be found between datasets
    * @return
    */
  def run(implicit spark: SparkSession, logger: ValidationLogger): TestResult = TestResult.makeTestResult{
    testType match {
      case sparktest.TestTypes.Count     => this.countTest
      case sparktest.TestTypes.Arrays    => this.arraysTest
      case sparktest.TestTypes.DDL       => this.ddlTest
      case sparktest.TestTypes.Constants => this.constantsTest
    }
  }(test = this, validationLogger = logger)

  override def toString: String =
    s"""Test number: $number Type: $testType
       |Source expression:
       |$sourceExpr
       |Target expression:
       |$targetExpr
       |Source path:
       |$sourcePath
       |Target path:
       |$targetPath
       |""".stripMargin

}

object SparkTest {

  /** Stores information about test-file
    * @param number
    *   number of test
    * @param destination
    *   destination: source or target
    * @param testType
    *   type of test (from TestTypes enum)
    * @param expr
    *   description of test (SQL-expression or constants), read from file
    * @param path
    *   path to test-file
    */
  case class FileInfo(number: Int,
                      destination: String,
                      testType: TestTypes.TestType,
                      expr: String,
                      path: Path
  )

  /** Create instance of SparkTest
    * @param number
    *   number of test
    * @param sourcePath
    *   path to test with source definition
    * @param targetPath
    *   path to test with target definition
    * @param sourceExpr
    *   expression of source test, read from file
    * @param targetExpr
    *   expression of target test, read from file
    * @param testType
    *   type of test from TestTypes enum
    */
  def make(number: Int,
           sourcePath: Path,
           targetPath: Path,
           sourceExpr: String,
           targetExpr: String,
           testType: TestTypes.TestType
  ): SparkTest = {
    SparkTest(number, sourcePath, targetPath, sourceExpr, targetExpr, testType)
  }

  /** Regex, used to parse test-file's names */
  private val testRegex =
    "^test_(?<number>\\d{1,3})_(?<destination>source|target)_(?<type>[A-Za-z]+)\\.sql$".r

  /** Parsing file name, read this file and creates FileInfo
    * @param path
    *   path to file
    * @param charset
    *   encoding of file
    */
  def makeFileInfo(path: Path, charset: Charset = StandardCharsets.UTF_8): FileInfo = {
    val fileName = path.getFileName.toString
    fileName match {
      case testRegex(number, destination, testType) =>
        val expr =
          if (inJar) readResource(s"/${path.toString}", charset)
          else readResource(path.toUri, charset)
        FileInfo(
          number.toInt,
          destination,
          TestTypes
            .fromString(testType)
            .getOrElse(throw new InvalidFileNameException(fileName)),
          expr,
          path
        )
      case _ => throw new InvalidFileNameException(fileName)
    }
  }

  /** Parsing file name, read this file and creates */
  def makeFileInfo(file: File): FileInfo = makeFileInfo(file.toPath)

  /** Create SparkTest from paths of source and target test-files */
  def fromPath(source: Path, target: Path): SparkTest = {
    val sourceInfo = makeFileInfo(source)
    val targetInfo = makeFileInfo(target)
    if (
      sourceInfo.number != targetInfo.number ||
      sourceInfo.testType != targetInfo.testType ||
      sourceInfo.destination == targetInfo.destination
    )
      throw new WrongTestPairException(source.toUri.toString, target.toUri.toString)
    else
      SparkTest.make(sourceInfo.number,
                     source,
                     target,
                     sourceInfo.expr,
                     targetInfo.expr,
                     sourceInfo.testType
      )

  }

}
