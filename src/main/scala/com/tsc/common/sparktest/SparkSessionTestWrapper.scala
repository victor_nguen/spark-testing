package com.tsc.common.sparktest

import org.apache.spark.sql.SparkSession

trait SparkSessionTestWrapper {
  implicit lazy val spark: SparkSession = SparkSession
    .builder()
    .master("local[4]")
    .appName("Spark Tests")
    .config("spark.ui.port", "12312")
    .getOrCreate()
}
