package com.tsc.common.sparktest

import com.tsc.common.functions.getResourceFolderFiles
import com.tsc.common.logging.factories.ValidationLoggerFactory
import com.tsc.common.logging.loggers.ValidationLogger
import com.tsc.exceptions._
import com.tsc.run.CommandLineParameters
import org.apache.log4j.LogManager
import org.apache.spark.sql.SparkSession

import java.io.File
import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future}

class TestExecutor extends App {

  protected lazy val config = CommandLineParameters.getParams(args)

  /** Parse and run all tests in folders, passed with -d (or --dirs) application parameter, or
    * in tests folder. Folder with tests must be located in 'resources' folder. Its possible to
    * run tests in parallel.
    * @param executionContext
    *   if ExecutionContext provided, tests will run in parallel
    * @param spark
    *   SparkSession used to load data and perform tests
    */
  def startAllTests(throwError: Boolean = true)(implicit
      spark: SparkSession,
      loggerFactory: ValidationLoggerFactory,
      executionContext: ExecutionContext
  ): Unit = {
    val testDirs = config.testDirs
    val tests: List[Either[Throwable, SparkTest]] = testDirs
      .flatMap(SparkTestParsingLogic.makeTests)
    val testOrder = (t: SparkTest) => (t.sourcePath, t.number, t.testType.toString)
    val invalidTests: List[Throwable] = tests.collect { case Left(e) => e }
    val validTests: List[SparkTest] = tests.view
      .collect { case Right(test) => test }
      .sortBy(testOrder)
      .toList

    val testResults = runTests(validTests)

    val result = constructMessage(testResults, invalidTests).map {
      case (exceptionMessageBuilder, failedTestsInfoExceptions) =>
        if ((invalidTests.nonEmpty || failedTestsInfoExceptions.nonEmpty) && throwError)
          throw new AirflowTestInfoException(exceptionMessageBuilder.mkString)
        else LogManager.getRootLogger.info(exceptionMessageBuilder.mkString)
    }
    Await.result(result, Duration.Inf)
  }

  /**
   * Makes message from test results and invalid parsed tests
   */
  def constructMessage(testResultsFuture: Future[List[TestResult]],
                       invalidTests: List[Throwable]
  )(implicit executionContext: ExecutionContext): Future[(StringBuilder, List[FailedTestInfoException])] = {

    val exceptionMessageBuilder = new StringBuilder

    if (invalidTests.nonEmpty)
      exceptionMessageBuilder ++= invalidTests
        .map(_.getMessage)
        .mkString("Invalid tests:\n", "\n", "\n")
    testResultsFuture.map { testResults =>
      val succeedTestsInfoExceptions: List[SucceedTestInfoException] = testResults
        .filter(_.isSucceed)
        .map(new SucceedTestInfoException(_))
      val failedTestsInfoExceptions: List[FailedTestInfoException] = testResults
        .filter(!_.isSucceed)
        .map(tr => new FailedTestInfoException(tr))

      exceptionMessageBuilder ++= succeedTestsInfoExceptions
        .map(_.getMessage)
        .mkString("Succeed tests:\n", "\n", "\n") ++= failedTestsInfoExceptions
        .map(_.getMessage)
        .mkString("Failed tests\n", "\n", "\n")

      (exceptionMessageBuilder, failedTestsInfoExceptions)
    }
  }

  /** Runs test in parallel and add logger that will handle error message to each test.
    * @param validTests
    *   tests to run
    * @param ec
    *   ExecutionContext to run tests
    * @return
    *   Map of TestResult and its logger with message
    */
  def runTests(validTests: List[SparkTest])(implicit
      sparkSession: SparkSession,
      ec: ExecutionContext,
      loggerFactory: ValidationLoggerFactory
  ): Future[List[TestResult]] = Future
    .sequence(validTests.map { test =>
      Future {
        implicit val logger: ValidationLogger = loggerFactory.getLogger
        test.run
      }
    })

}
