package com.tsc.common.sparktest

import com.tsc.common.functions.getResourceFolderFiles
import com.tsc.exceptions.TestParsingException

import java.io.File

object SparkTestParsingLogic {

  /** Parsing files in given directory and creates List of tests.
    * @param pathToTests
    *   path to folder with test, located in resources
    * @return
    *   List of Either[Throwable, SparkTest]. If parsing correct, making Right(SparkTest), if
    *   parsing fails, making Left(Throwable).
    */
  protected[sparktest] def makeTests(
      pathToTests: String
  ): List[Either[Throwable, SparkTest]] = {
    val files: Option[List[File]] = getResourceFolderFiles(pathToTests).map {
      _.filter(f => f.toString.endsWith("sql"))
    }
    files match {
      case Some(listFiles) =>
        listFiles
          .map(SparkTest.makeFileInfo)
          .groupBy(info => (info.number, info.testType))
          .map(processTestGroup)
          .toList
      case None => List()
    }
  }

  def processTestGroup(
      m: ((Int, TestTypes.TestType), List[SparkTest.FileInfo])
  ): Either[TestParsingException, SparkTest] = m match {
    case ((number, testType), listInfo) if !(listInfo.length == 2) =>
      Left(
        new TestParsingException(
          s"""Must be 2 files with same number: source and target.
             | Found: ${listInfo.length} files for test number - $number with type - $testType""".stripMargin
        )
      )
    case ((number, testType), listInfo)
        if !(listInfo.count(_.destination == "source") == 1 &&
          listInfo.count(_.destination == "target") == 1) =>
      Left(
        new TestParsingException(
          s"""Must be 1 source and 1 destination file for every test with same number and type.
             | Exception for test number - $number with type - $testType""".stripMargin
        )
      )
    case ((number, testType), listInfo) =>
      val sourceInfo = listInfo.filter(_.destination == "source").head
      val targetInfo = listInfo.filter(_.destination == "target").head
      Right(
        SparkTest.make(number,
                       sourceInfo.path,
                       targetInfo.path,
                       sourceInfo.expr,
                       targetInfo.expr,
                       testType
        )
      )
  }
}
