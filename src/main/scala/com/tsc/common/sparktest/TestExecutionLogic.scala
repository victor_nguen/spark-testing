package com.tsc.common.sparktest

import com.tsc.common.logging.loggers.ValidationLogger
import com.tsc.implicits.DatasetTestOps
import org.apache.spark.sql.{DataFrame, SparkSession}

import scala.util.Random

object TestExecutionLogic {

  def processCountTest(test: SparkTest)(implicit
      spark: SparkSession,
      logger: ValidationLogger
  ): Boolean = {
    val left  = spark.sql(test.sourceExpr)
    val right = spark.sql(test.targetExpr)
    left.compareEqualityByCountTo(right)
  }

  def processArraysTest(test: SparkTest)(implicit
      spark: SparkSession,
      logger: ValidationLogger
  ): Boolean = {
    val left  = spark.sql(test.sourceExpr)
    val right = spark.sql(test.targetExpr)
    left.compareEqualityPreciselyTo(right)
  }

  def processDdlTest(
      test: SparkTest
  )(implicit spark: SparkSession, logger: ValidationLogger): Boolean = {
    import spark.implicits._
    val left: DataFrame = spark.sql(test.targetExpr)
    // reading lines from source test-file
    val lines = test.sourceExpr
      .split("\n")
      .map(_.replace("\r", "").trim)
      .toList

    // transforming read lines to describe-like Dataframe
    val right = lines
      .map(_.split(";").take(2))
      .map(arr => Tuple2.apply(arr(0), arr(1)))
      .toDF("col_name", "data_type")
    // comparing dataframes
    left
      .select("col_name", "data_type")
      .compareDdlTo(
        right.select("col_name", "data_type")
      )
  }
  def processConstantsTest(test: SparkTest)(implicit
      spark: SparkSession,
      logger: ValidationLogger
  ): Boolean = {
    val left = spark.sql(test.sourceExpr)
    // reading lines from target test-file and splitting them
    val constants: List[Array[String]] = test.targetExpr.split("\n").toList.map(_.split(";"))
    // making SQL-expression to evaluate via spark.sql() and get Dataframe, quantity of columns is not known,
    // so we can't transform it with toDF() and this crutch is used.
    // columns naming randomly
    val targetSql =
      (
        constants.head.map(c => s"$c as ${Random.alphanumeric.take(10).mkString}") +:
          constants.tail
      )
        .map(c => s"SELECT ${c.mkString(", ")}")

    val right =
      targetSql.tail.foldLeft(spark.sql(targetSql.head))((acc, el) => acc.union(spark.sql(el)))

    left.compareEqualityPreciselyTo(right)
  }

}
