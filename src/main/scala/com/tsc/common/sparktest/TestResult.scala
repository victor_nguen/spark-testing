package com.tsc.common.sparktest

import com.tsc.common.logging.loggers.ValidationLogger
import com.tsc.exceptions.AnalysisSimpleException

case class TestResult(isSucceed: Boolean,
                      test: SparkTest,
                      exception: Option[Exception] = None,
                      logger:ValidationLogger
)

object TestResult{
  def makeTestResult(f: => Boolean)(test: SparkTest, validationLogger: ValidationLogger): TestResult = {
    try {
      val result = f
      TestResult(result, test, None, validationLogger)
    } catch {
      case ae: org.apache.spark.sql.AnalysisException =>
        TestResult(isSucceed = false,
          test = test,
          exception = Some(new AnalysisSimpleException(ae.getSimpleMessage)),
          validationLogger
        )
      case e: Exception =>
        TestResult(isSucceed = false, test = test, exception = Some(e), validationLogger)
    }
  }
}
