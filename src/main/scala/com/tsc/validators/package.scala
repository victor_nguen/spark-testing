package com.tsc

import com.tsc.common.logging.loggers.{FileValidationLogger, ValidationLogger}
import com.tsc.implicits._
import exceptions.UnknownLoggerException
import org.apache.spark.rdd.RDD
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Dataset, Encoder, Encoders, Row, SparkSession}

import java.text.{ParseException, SimpleDateFormat}
import java.time.format.DateTimeFormatter
import java.util.{Locale, UUID}
import scala.language.postfixOps
import scala.reflect.ClassTag
import scala.util.Random

package object validators {

  /** Comparing two datasets and return true if they are equal.
    *
    * To reduce network load the following idea is used:
    *
    *   1. If the table has a unique identifier, it is used later, if not, a list of fields is
    *      transmitted by which two tables can be sorted so that the records in them are in the
    *      same order after sorting. Rows are numbered after sorting and number is used as id.
    *
    * 2. The hash of each row is calculated based on values of columns.
    *
    * 3. Joining two tables using inner join. If the number of rows in the joined table is
    * equal to the number of rows in one of the compared tables, then they are equal.
    *
    * @param left
    *   first Dataset
    * @param right
    *   Dataset to compare with
    * @param leftDfId
    *   id column name of first Dataset
    * @param rightDfId
    *   id column name of second Dataset
    * @param count
    *   count of records
    * @param hFunc
    *   name of hash function to use, can be murmur, md5, sha-1, sha-2-[224, 256, 384, 512].
    *
    * When using sha-2, second number is a number of bits using for encryption. Example:
    * sha-2-512.
    *
    * Case insensitive.
    * @return
    *   true if datasets are equal, otherwise false
    */
  private def _assertEqualityWithHashing[A](left: Dataset[A],
                                            right: Dataset[A],
                                            leftDfId: String,
                                            rightDfId: String,
                                            count: Long,
                                            hFunc: String
  )(implicit logger: ValidationLogger): Boolean = {
    // names of computed columns with hash, chosen to be unique column names
    val leftHashColName  = makeNewColName("left_hash_digest")
    val rightHashColName = makeNewColName("right_hash_digest")
    val lId              = s"l_$leftDfId"
    val rId              = s"r_$rightDfId"

    // computing hash with given hash function
    val (leftDfWithHash, rightDfWithHash) = hFunc.toLowerCase match {
      case "murmur" =>
        (
          left.withColumn(leftHashColName,
                          hash(left.columns.filterNot(_ == leftDfId).map(col): _*)
          ),
          right.withColumn(rightHashColName,
                           hash(right.columns.filterNot(_ == rightDfId).map(col): _*)
          )
        )
      case "md5" =>
        (
          left.withColumn(leftHashColName, md5(concat_ws(",", left.columns.map(col): _*))),
          right.withColumn(rightHashColName, md5(concat_ws(",", right.columns.map(col): _*)))
        )
      case "sha-1" =>
        (
          left.withColumn(leftHashColName, sha1(concat_ws(",", left.columns.map(col): _*))),
          right.withColumn(rightHashColName, sha1(concat_ws(",", right.columns.map(col): _*)))
        )
      case x if x.startsWith("sha-2") =>
        val funcName = "sha-2-(\\d\\d\\d)".r
        val numBits: Int = x match {
          case funcName(bits) =>
            val nBits = bits.toInt
            if (List(224, 256, 384, 512).contains(nBits)) nBits
            else
              throw new Exception(
                "if using SHA-2, num of bits should be in [224, 256, 384, 512]. Example: sha-2-512"
              )
          case _ =>
            throw new Exception(
              "Invalid hash function name. See valid function names in docs"
            )
        }
        (
          left.withColumn(leftHashColName,
                          sha2(concat_ws(",", left.columns.map(col): _*), numBits)
          ),
          right.withColumn(rightHashColName,
                           sha2(concat_ws(",", right.columns.map(col): _*), numBits)
          )
        )
    }
    // making new columns with hash on left and right tables
    val leftHash = leftDfWithHash
      .select(leftDfId, leftHashColName)
      .withColumnRenamed(leftDfId, lId)
    val rightHash = rightDfWithHash
      .select(rightDfId, rightHashColName)
      .withColumnRenamed(rightDfId, rId)

    // joining Datasets (inner)
    val joinedHashes =
      leftHash
        .join(rightHash,
              leftHash.col(lId) === rightHash.col(rId) &&
                leftHash.col(leftHashColName) === rightHash.col(rightHashColName),
              "outer"
        )
    // find difference and logging it
    // null in id (left or right) after outer join means that rows have different calculated hash

    // find difference in hashes
    val hashDiff = joinedHashes
      .filter(col(lId).isNull || col(rId).isNull)
    // making Datasets with rows that differ in left and right Dataset
    lazy val leftDiff = hashDiff
      .join(left, hashDiff.col(lId) === left.col(leftDfId))
      .drop(hashDiff.columns: _*)
      .drop(leftDfId)
    lazy val rightDiff = hashDiff
      .join(right, hashDiff.col(rId) === right.col(rightDfId))
      .drop(hashDiff.columns: _*)
      .drop(rightDfId)
    try {
      joinedHashes.cache()
      leftDiff.cache()
      rightDiff.cache()

      val joinedHashesCount = joinedHashes.count()
      // if inner join returned same quantity of rows as each of table by themself => Datasets are equals
      // else find difference and log it
      if (joinedHashesCount == count) return true

      // logging difference in Datasets
      val lDiff = leftDiff.except(rightDiff).sort(leftDiff.columns.map(col): _*)
      val rDiff = rightDiff.except(leftDiff).sort(rightDiff.columns.map(col): _*)

      if (lDiff.isEmpty && rDiff.isEmpty) return true

      logger match {
        case logger: FileValidationLogger =>
          logger.write(lDiff.withColumn("index", monotonically_increasing_id()),
                       rDiff.withColumn("index", monotonically_increasing_id()),
                       "index",
                       "index"
          )
        case logger: ValidationLogger => logger.write(lDiff, rDiff)
        case _ =>
          throw new UnknownLoggerException(
            "logger should inherit trait ValidationLogger"
          )
      }
      false
    } finally {
      joinedHashes.unpersist()
      leftDiff.unpersist()
      rightDiff.unpersist()
    }
  }

  /**
   * Checking two datasets for equality using exceptAll
   */
  def assertEqualityPrecisely[A](left: Dataset[A], right: Dataset[A])(implicit
                                                                      logger: ValidationLogger
  ): Boolean = {
    try {
      left.cache()
      right.cache()
//    val leftCount  = left.count()
//    val rightCount = right.count()
//    if (leftCount != rightCount) return false
      val leftDiff  = left.exceptAll(right)
      val rightDiff = right.exceptAll(left)
      // if leftDiff and rightDiff is empty -> datasets are equal
      val isEqual = leftDiff.isEmpty && rightDiff.isEmpty
      if (!isEqual)
        logger.write(leftDiff.sort(leftDiff.columns.map(col): _*),
                     rightDiff.sort(rightDiff.columns.map(col): _*)
        )
      isEqual
    } finally {
      left.unpersist()
      right.unpersist()
    }
  }

  def compareDDl[A](left: Dataset[A], right: Dataset[A])(implicit
      logger: ValidationLogger
  ): Boolean = try {
    left.cache()
    right.cache()
    val l         = left.select(lower(concat_ws(";", left.columns.map(col): _*)))
    val r         = right.select(lower(concat_ws(";", right.columns.map(col): _*)))
    val leftDiff  = l.except(r)
    val rightDiff = r.except(l)
    val isEqual   = leftDiff.isEmpty && rightDiff.isEmpty
    if (!isEqual)
      logger.write(leftDiff.sort(leftDiff.columns.map(col): _*),
                   rightDiff.sort(rightDiff.columns.map(col): _*)
      )
    isEqual
  } finally {
    left.unpersist()
    right.unpersist()
  }

  /** Comparing datasets by joining two RDD and comparing pairs of records
    * @note
    *   Very slow, should be used only for small datasets
    */
  @deprecated("very slow implementation of comparison, should not be used")
  def assertEqualityByRDD[A : ClassTag](left: Dataset[A], right: Dataset[A]): Boolean = {
    val leftColumns                  = left.columns.map(col)
    val rightColumns                 = right.columns.map(col)
    val leftIndexed: RDD[(Long, A)]  = zipWithIndex(left.sort(leftColumns: _*).rdd)
    val rightIndexed: RDD[(Long, A)] = zipWithIndex(right.sort(rightColumns: _*).rdd)
    _assertEqualityByRDD(leftIndexed, rightIndexed)
  }

  def assertEqualityByRDD[A : ClassTag](left: RDD[A], right: RDD[A]): Boolean =
    _assertEqualityByRDD(zipWithIndex(left), zipWithIndex(right))

  private def _assertEqualityByRDD[A : ClassTag](left: RDD[(Long, A)],
                                                 right: RDD[(Long, A)]
  ): Boolean = {
    val unequalRDD = left
      .join(right)
      .filter { case (_, (leftRow, rightRow)) =>
        leftRow != rightRow
      }
    if (unequalRDD.isEmpty) true
    else false
  }

  /** RDD.zipWithIndex, but adding index column first */
  private def zipWithIndex[T](rdd: RDD[T]): RDD[(Long, T)] = {
    rdd.zipWithIndex().map { case (row, idx) =>
      (idx, row)
    }
  }

  /** Comparing two datasets and return true if they are equal.
    *
    * @param left
    *   first Dataset
    * @param right
    *   Dataset to compare with
    * @param leftDfId
    *   id column name of first Dataset
    * @param rightDfId
    *   id column name of second Dataset count of records
    * @param hFunc
    *   name of hash function to use, can be murmur, md5, sha-1, sha-2-[224, 256, 384, 512].
    *
    * When using sha-2, second number is a number of bits using for encryption. Example:
    * sha-2-512.
    *
    * Case insensitive.
    * @return
    *   true if datasets are equal, otherwise false
    */
  def assertEquality[A](left: Dataset[A],
                        right: Dataset[A],
                        leftDfId: String,
                        rightDfId: String,
                        hFunc: String = "murmur"
  )(implicit logger: ValidationLogger): Boolean = {

    // count of left and right tables
    val leftCount  = left.count()
    val rightCount = right.count()
    // if count of rows not equals between left and right tables => return false without computing hash and joining
    if (leftCount != rightCount) return false

    _assertEqualityWithHashing(left, right, leftDfId, rightDfId, leftCount, hFunc)
  }

  /** Comparing two datasets and return true if they are equal. Using Murmur3 hash function, if
    * needed to use another, see overloaded variants of function.
    *
    * @param left
    *   first Dataset
    * @param right
    *   Dataset to compare with
    * @param id
    *   id column name of Dataset, if id names are equal in both tables(Datasets)
    *
    * @return
    *   true if datasets are equal, otherwise false
    */
  def assertEquality[A](left: Dataset[A], right: Dataset[A], id: String)(implicit
      logger: ValidationLogger
  ): Boolean =
    assertEquality(left, right, id, id)

  /** Comparing two datasets and return true if they are equal.
    *
    * @param left
    *   first Dataset
    * @param right
    *   Dataset to compare with
    * @param leftSortColumns
    *   a list of column names of left table by which you can uniquely sort (so that records in
    *   both tables are sorted in exactly the same way)
    * @param rightSortColumns
    *   same as '''leftSortColumns''' for right table
    * @param hFunc
    *   name of hash function to use, can be murmur, md5, sha-1, sha-2-[224, 256, 384, 512].
    *
    * When using sha-2, second number is a number of bits using for encryption. Example:
    * sha-2-512.
    *
    * Case insensitive.
    * @return
    *   true if datasets are equal, otherwise false
    */
  def assertEquality[A](left: Dataset[A],
                        right: Dataset[A],
                        leftSortColumns: List[String],
                        rightSortColumns: List[String],
                        hFunc: String
  )(implicit logger: ValidationLogger): Boolean = {
    // count of left and right tables
    val leftCount  = left.count()
    val rightCount = right.count()
    // if count of rows not equals between left and right tables => return false without computing hash and joining
    if (leftCount != rightCount) return false

    // names of computed columns with hash, chosen to be unique column names
    val leftGeneratedPkColName  = makeNewColName("left_generated_id")
    val rightGeneratedPkColName = makeNewColName("right_generated_id")

    // making new columns with primary key and hash on left and right tables
    val leftWindow  = Window.orderBy(leftSortColumns.sorted.map(col): _*)
    val rightWindow = Window.orderBy(rightSortColumns.sorted.map(col): _*)
    _assertEqualityWithHashing(
      left.withColumn(leftGeneratedPkColName, row_number.over(leftWindow)),
      right.withColumn(rightGeneratedPkColName, row_number.over(rightWindow)),
      leftGeneratedPkColName,
      rightGeneratedPkColName,
      leftCount,
      hFunc
    )
  }

  /** Comparing two datasets and return true if they are equal.
    *
    * @param left
    *   first Dataset
    * @param right
    *   Dataset to compare with
    * @param leftSortColumns
    *   a list of column names of left table by which you can uniquely sort (so that records in
    *   both tables are sorted in exactly the same way)
    * @param rightSortColumns
    *   same as '''leftSortColumns''' for right table
    *
    * @return
    *   true if datasets are equal, otherwise false
    */
  def assertEquality[A](left: Dataset[A],
                        right: Dataset[A],
                        leftSortColumns: List[String],
                        rightSortColumns: List[String]
  )(implicit logger: ValidationLogger): Boolean =
    assertEquality(left, right, leftSortColumns, rightSortColumns, "murmur")

  /** Comparing two datasets and return true if they are equal.
    *
    * @param left
    *   first Dataset
    * @param right
    *   Dataset to compare with
    * @param compoundPk
    *   a list of column names table by which you can uniquely sort (so that records in both
    *   tables are sorted in exactly the same way)
    * @return
    *   true if datasets are equal, otherwise false
    */
  def assertEquality[A](left: Dataset[A], right: Dataset[A], compoundPk: List[String])(implicit
      logger: ValidationLogger
  ): Boolean =
    assertEquality(left, right, compoundPk, compoundPk)

  /** Comparing two datasets and return true if they are equal. Using all columns to sort
    * datasets
    * @param left
    *   first Dataset
    * @param right
    *   Dataset to compare with
    * @return
    *   true if datasets are equal, otherwise false
    */
  def assertEquality[A](left: Dataset[A], right: Dataset[A])(implicit
      logger: ValidationLogger
  ): Boolean =
    assertEquality(left, right, left.columns.toList, right.columns.toList)

  /** Asserting equality of two datasets by checking their quantity of rows
    * @param left
    *   first Dataset to compare
    * @param right
    *   second Dataset
    * @return
    *   true if quantity of values are equal for both datasets
    */
  def assertEqualityByCount[A](left: Dataset[A], right: Dataset[A])(implicit
      validationLogger: ValidationLogger
  ): Boolean = {
    val leftCount  = left.count()
    val rightCount = right.count()
    val result     = leftCount == rightCount
    if (!result)
      validationLogger.info(
        s"Source table contains: $leftCount rows, target table contains: $rightCount rows"
      )
    result
  }

  /** Validates that all strings match regex-pattern.
    * @param df
    *   Dataset to validate
    * @param column
    *   name of column to validate
    * @param regex
    *   regex pattern
    */
  def assertColumnByRegex[A](df: Dataset[A], column: String, regex: String)(implicit
      logger: ValidationLogger
  ): Boolean = {
    val newColName = makeNewColName("correspond_to_pattern")
    val dfWithCorrColumn = df.withColumn(
      newColName,
      when(df.col(column).rlike(regex), lit(true))
        .otherwise(lit(false))
    )
    val notCorrespondDf = dfWithCorrColumn
      .filter(col(newColName) === false)
      .drop(newColName)
      .cache
    val result = notCorrespondDf.isEmpty
    if (!result) logger.write(notCorrespondDf)
    result
  }

  /** Validates that date and time format on the column matches given format. Uses UDF so takes
    * SparkSession implicitly.
    * @param df
    *   Dataset to validate
    * @param colName
    *   name of column to validate
    * @param dateFormat
    *   [[https://help.gooddata.com/cloudconnect/manual/date-and-time-format.html Java date and time format]]
    * @param locale
    *   used Locale class
    * @param maxDeviation
    *   Maximum deviation, default = 0.0, that is, all values must comply with the specified
    *   format, you can specify a value > 0, in which case '''[number of rows] *
    *   maxDeviation''' rows with an inappropriate date and time format will be allowed
    *
    * Максимальное отклонение, по умолчанию = 0.0, то есть все значения должны соответствовать
    * заданному формату, можно указать значение > 0, в этом случае будет допустимо
    * '''[количество строк] * maxDeviation''' строк с не соответствующим форматом даты и
    * времени
    * @return
    */
  def assertDateTimeFormat[A](df: Dataset[A],
                              colName: String,
                              dateFormat: String,
                              locale: Locale = Locale.getDefault,
                              maxDeviation: Double = 0.0
  )(implicit
      spark: SparkSession,
      logger: ValidationLogger
  ): Boolean = {
    // create and register udf
    // Used DateTimeFormatter and SimpleDateFormat, because they don't parse some locale-specific date records separately
    // Example: DateTimeFormatter can't parse "17.май.21" and can parse "17.авг.21",
    //    on the other hand SimpleDateFormat can parse "17.май.21" and can't parse "17.авг.21"
    val validateDateTime = udf { str: String =>
      try {
        DateTimeFormatter.ofPattern(dateFormat, locale).parse(str)
        true
      } catch {
        case _: java.time.format.DateTimeParseException =>
          try {
            new SimpleDateFormat(dateFormat, locale).parse(str)
            true
          } catch {
            case _: ParseException => false
          }
      }
    }
    spark.udf.register("validateDateTime", validateDateTime)
    val newColName = makeNewColName("isValidDateTimeFormat")
    // number of dates in wrong format
    val notMatched = df
      .withColumn(
        newColName,
        expr(s"validateDateTime($colName)")
      )
      .filter(col(newColName) === false)
      .drop(newColName)
      .cache
    val notMatchedIsEmpty = notMatched.isEmpty
    if (!notMatchedIsEmpty) logger.write(notMatched)
    // compare with 0 if maxDeviation is not set, otherwise calculating ratio and comparing with it
    if (maxDeviation == 0.0) notMatchedIsEmpty
    else notMatched.count() <= df.count() * maxDeviation
  }

  /** Creates a unique column name using the UUID. Used to prevent collisions with field names
    * in the tested Dataset.
    * @param name
    *   name using as prefix
    */
  private def makeNewColName(name: String, len: Int = 9): String =
    s"$name-${Random.alphanumeric take len mkString}"

  /** @see
    *   [[validators#makeNewColName(java.lang.String):java.lang.String]]
    */
  private def makeNewColName: String = makeNewColName("generated_column")

}
