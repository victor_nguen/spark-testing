package com.tsc.run

import com.tsc.common.logging.factories.{StdValidationLoggerFactory, ValidationLoggerFactory}
import com.tsc.common.sparktest.TestExecutor
import org.apache.spark.SparkConf
import org.apache.spark.sql.SparkSession

import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext

/** Used to run tests locally */
object LocalRun extends TestExecutor {
  // for testing purposes only
  private def createTempViews()(implicit spark: SparkSession): Unit = {
    val dataFolderPath = "datamarts"
//    val dataFolderPath = "/usr/local/spark/resources/data"
    List("Accounts", "Companies", "Operations", "Users", "dm_sum_operations")
      .foreach { name =>
        spark.read
          .parquet(s"$dataFolderPath/dm_sum_operations/$name.parquet")
          .cache
          .createOrReplaceTempView(name)
      }

    List("bank",
         "client",
         "client_tr",
         "document",
         "payment_system",
         "tr_sum_ch",
         "transactions",
         "dm_mosttr_per_onebankclient",
         "dm_sber_tramount_per_day"
    ).foreach { name =>
      spark.read
        .parquet(s"$dataFolderPath/bnk_clnt_tr/$name.parquet")
        .cache
        .createOrReplaceTempView(name)
    }
  }

  def startAllTestsLocally(
  )(implicit loggerFactory: ValidationLoggerFactory,
    executionContext: ExecutionContext): Unit = {
    val sparkConf = new SparkConf()
//      .set("spark.executor.instances", "2")
      .set("spark.ui.port", "4545")
      .set("spark.sql.shuffle.partitions", "20")
//      .set("spark.executor.memory", "1G")
//      .set("spark.executor.cores", "1")
      .set("spark.driver.memory", "1G")

    implicit val spark: SparkSession = SparkSession
      .builder()
      .master("local[2]")
      .config(sparkConf)
      .appName("Run tests on data marts")
      .getOrCreate()
//    spark.sparkContext.setLogLevel("ERROR")

    createTempViews()
    spark.time {
      startAllTests(throwError = false)(spark, loggerFactory, executionContext)
    }
  }

  implicit val ec: ExecutionContext = config.executionContext
  implicit val loggerFactory: ValidationLoggerFactory = StdValidationLoggerFactory

  startAllTestsLocally()
  Thread.sleep(1000000000)
}
