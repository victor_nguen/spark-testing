package com.tsc.run

import scopt.OParser

import java.util.concurrent.Executors
import scala.concurrent.ExecutionContext
import scala.util.matching.Regex

object CommandLineParameters {
  // creating config using 'scopt'
  case class ExecutorParams(testDirs: List[String] = List.empty,
                            executionContext: ExecutionContext =
                              ExecutionContext.fromExecutor(Executors.newSingleThreadExecutor)
  )
  private val builder = OParser.builder[ExecutorParams]
  private val configParser = {
    import builder._
    OParser.sequence(
      programName("SparkTesting"),
      head("TestExecutor"),
      opt[Seq[String]]('d', "dirs")
        .valueName("<dir1>,<dir2>...")
        .optional()
        .action((param, conf) => conf.copy(testDirs = param.toList))
        .text("directories with test files"),

      opt[String]('e', "executionContext")
        .valueName(
          "cachedThreadPool | fixedThreadPool=n | workSteelingPool=n | singleThread"
        )
        .optional()
        .action(makeExecutionContextParam)
        .text("ExecutionContext, that will be used to run tests, default is singleThread")
    )
  }
  val ecRegex: Regex =
    "^(?<poolType>fixedThreadPool|workSteelingPool)=(?<nThreads>\\d{1,2})$".r

  def makeExecutionContextParam: (String, ExecutorParams) => ExecutorParams =
    (param: String, conf: ExecutorParams) => {
      val executorService = param match {
        case ecRegex(poolType, nThreads) =>
          poolType match {
            case "fixedThreadPool"  => Executors.newFixedThreadPool(nThreads.toInt)
            case "workSteelingPool" => Executors.newWorkStealingPool(nThreads.toInt)
          }
        case "cachedThreadPool" => Executors.newCachedThreadPool()
        case "singleThread"     => Executors.newSingleThreadExecutor()
      }
      conf.copy(executionContext = ExecutionContext.fromExecutor(executorService))
    }

  def getParams(args: Array[String]): ExecutorParams =
    OParser.parse(configParser, args, ExecutorParams()) match {
      case Some(config) => config
      case None         => ExecutorParams()
    }
}
