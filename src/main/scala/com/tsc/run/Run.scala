package com.tsc.run

import com.tsc.common.logging.factories.{StdValidationLoggerFactory, ValidationLoggerFactory}
import com.tsc.common.sparktest.TestExecutor
import org.apache.spark.sql.SparkSession

import scala.concurrent.ExecutionContext

/**
 * Starts running tests
 */
object Run extends TestExecutor {

  implicit val spark: SparkSession = SparkSession
    .builder()
    .getOrCreate()

  implicit val loggerFactory: ValidationLoggerFactory = StdValidationLoggerFactory
  implicit val ec: ExecutionContext = config.executionContext

  startAllTests()
}
