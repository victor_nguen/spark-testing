import com.tsc.common.functions.getResource
import com.tsc.common.logging.loggers.{DebugValidationLogger, ValidationLogger}
import com.tsc.common.sparktest.SparkSessionTestWrapper
import com.tsc.implicits._
import com.tsc.validators._
import org.apache.log4j.{LogManager, Logger}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{DataFrame, DataFrameReader}
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

import java.util.Locale

class ValidatorsBehaviour extends AnyFlatSpec with SparkSessionTestWrapper with Matchers {

  spark.sparkContext.setLogLevel("ERROR")

  lazy val logger: Logger = LogManager.getRootLogger

  lazy val defaultCsvReader: DataFrameReader = spark.read
    .option("delimiter", ";")
    .option("encoding", "UTF-8")
    .option("inferSchema", "true")
    .option("header", "true")
    .option("ignoreLeadingWhiteSpace", "true")
    .option("ignoreTrailingWhiteSpace", "true")

  lazy val bankTestDf: DataFrame = defaultCsvReader
    .csv(getResource("test_data/bank.csv").getPath)
    .sort()

  lazy val bankTestDfPlusLine: DataFrame = defaultCsvReader
    .csv(getResource("test_data/bank_plusOneLine.csv").getPath)
    .sort()

  lazy val bankTestDfCorrupted: DataFrame = defaultCsvReader
    .csv(getResource("test_data/bank_withCorruptedLine.csv").getPath)
    .sort()

  implicit lazy val debugLogger: ValidationLogger = new DebugValidationLogger
//  implicit lazy val csvFileLogger: ValidationLogger =
//    new BasicFileValidationLogger("src/main/resources/test_logs/test.orc",
//                              "orc",
//                              SaveMode.Append,
//                              Map("header" -> "true")
//    )
//  implicit lazy val debugLogger: ValidationLogger = new StdValidationLogger(logger)

  behavior of "assertEquality"

  it should "return true when comparing dataset to itself (with passing names of columns to sort by)" in {
    assertEquality(bankTestDf, bankTestDf) should be(true)
  }

  it should "return true when comparing dataset to itself (with id column passed)" in {
    val testDfWithId = bankTestDf.withColumn("id", monotonically_increasing_id())
    assertEquality(testDfWithId, testDfWithId, "id", "id") should be(true)
  }

  it should "return false when comparing dataset with not equal quantity of rows" in {
    assertEquality(bankTestDf,
                   bankTestDfPlusLine,
                   List("Account No",
                        "TRANSACTION DETAILS",
                        "VALUE DATE",
                        "CHQ_NO",
                        "WITHDRAWAL AMT",
                        "DEPOSIT AMT",
                        "BALANCE AMT"
                   )
    ) should be(false)
  }

  it should "return false when comparing dataset with another one with one different line" in {
    assertEquality(
      bankTestDf,
      bankTestDfCorrupted
    ) should be(false)
  }

  behavior of "assertColumnByRegex"

  it should "return true when validating regex pattern on column of valid dataset" in {
    // implicit conversion
    val res = bankTestDf.assertPattern("Account No", """^\d{1,12}'$""")
    res should be(true)
  }

  it should "return false when validating regex pattern on column of corrupted dataset" in {
    val res = bankTestDfCorrupted.assertPattern("Account No", "^\\d{1,12}'$")
    res should be(false)
  }

  behavior of "assertDateFormat"

  it should "return true when validating DateTime format on string column of valid dataset" in {
    val res =
      bankTestDf.assertDateFormat("DATE", "dd.MMM.yy", Locale.forLanguageTag("ru"))
    res should be(true)
  }

  it should "return false when validating DateTime format on string column of corrupted dataset" in {
    val res =
      bankTestDfCorrupted.assertDateFormat("DATE",
                                           "dd.MMM.yy",
                                           Locale.forLanguageTag("ru")
      )
    res should be(false)
  }

  behavior of "_assertEqualityPrecisely"
  it should "return true when comparing dataset to itself" in {
    assertEqualityPrecisely(bankTestDf, bankTestDf) should be(true)
  }

  it should "return false when comparing dataset with other with corrupted record" in {
    assertEqualityPrecisely(bankTestDf, bankTestDfCorrupted) should be(false)
  }

//  behavior of "_assertEqualityByRDD"
//
//  it should "return true when comparing equal datasets" in {
//    _assertEqualityByRDD(bankTestDf, bankTestDf) should be(true)
//  }
//
//  it should "return false when comparing unequal datasets" in {
//    _assertEqualityByRDD(bankTestDf, bankTestDfCorrupted) should be(false)
//  }
}
