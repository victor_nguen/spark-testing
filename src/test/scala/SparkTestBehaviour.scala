import com.tsc.common.functions.getResource
import com.tsc.common.sparktest.{SparkTest, TestTypes}
import com.tsc.exceptions._
import org.scalatest.flatspec.AnyFlatSpec

import java.nio.file.Path

class SparkTestBehaviour extends AnyFlatSpec {

  val testsFolder = "sql_tests"

  behavior of "fromPath"

  it should "create new SparkTest object with valid type of test and number" in {
    val source: Path = getResource(s"$testsFolder/test_1_source_arrays.sql").toPath
    val target: Path = getResource(s"$testsFolder/test_1_target_arrays.sql").toPath
    val res          = SparkTest.fromPath(source, target)
    assert(res.number > 0 && res.testType == TestTypes.Arrays)
  }

  it should "throw an WrongTestPair when trying to create test from incompatible by type pair of files" in {
    val source: Path = getResource(s"$testsFolder/test_1_source_arrays.sql").toPath
    val target: Path = getResource(s"$testsFolder/test_1_target_counts.sql").toPath

    assertThrows[WrongTestPairException](
      SparkTest.fromPath(source, target)
    )
  }

  it should "throw an WrongTestPair when trying to create test from incompatible by number pair of files" in {
    val source: Path = getResource(s"$testsFolder/test_1_source_arrays.sql").toPath
    val target: Path = getResource(s"$testsFolder/test_2_target_counts.sql").toPath

    assertThrows[WrongTestPairException](
      SparkTest.fromPath(source, target)
    )
  }
}
