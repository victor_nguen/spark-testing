ThisBuild / version := "1.1.0"

ThisBuild / scalaVersion := Dependencies.V.scala

ThisBuild / organization := "com.tsc"


lazy val root = (project in file("."))
  .settings(
    name := "SparkTesting"
  )
  .settings(BuildSettings.assemblySettings)
  .settings(libraryDependencies ++= Dependencies.all)
