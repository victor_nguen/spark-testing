import sbt._

object Dependencies {

  object V {
    // Scala
    val scala = "2.11.12"

    // read config
    val tsConfig = "1.4.2"

    //parse args
    val scopt = "4.0.1"

    // spark
    val spark =
      "2.4.7"
//      "3.1.2"

    // Unit tests
    val scalatest  = "3.2.12"
  }

  val commonDependencies = List(
    "com.typesafe" % "config" % V.tsConfig,
    "com.github.scopt" %% "scopt" % V.scopt
  )

  val sparkDependencies = List(
    "org.apache.spark" %% "spark-core" % V.spark % "provided",
    "org.apache.spark" %% "spark-sql"  % V.spark % "provided",
    "org.apache.spark" %% "spark-hive" % V.spark % "provided"
  )

  val testDependencies = List(
    "org.scalactic" %% "scalactic" % V.scalatest,
    "org.scalatest" %% "scalatest" % V.scalatest % "test"
  )

  val all = sparkDependencies ++
    testDependencies ++
    commonDependencies

}
