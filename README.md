# Spark Testing

- [Функциональность](#функциональность)
- [Структура проекта](#структура-проекта-и-пакетов)
- [Формат и описание тестов](#формат-и-описание-тестов)
    - [Arrays](#arrays)
    - [Counts](#counts)
    - [DDL](#ddl)
    - [Constants](#constants)
- [Использование](#использование)
    - [Запуск тестов расположенных в директории resources](#запуск-тестов,-расположенных-в-папке-resources)
        - [Последовательно в одном потоке](#в-одном-потоке)
        - [Параллельно](#параллельно)
        - [Создание собственного класса/объекта](#Создание-собственного-класса/объекта)
        - [Параметры командной строки](#параметры-командной-строки)
    - [Использование функиций для написания тестов на scala](#использование-функций-для-написания-тестов-на-scala)
- [Примеры запуска](#пример-запуска)

## Функциональность

- [X] Считывание файлов тестов из директории 'resources' и запуск их выполнения.
- [X] Можно использовать как библиотеку для написания тестов с помощью Spark API

## Структура проекта и пакетов

- project - директория с конфигурационными фалами проекта
    - BuildSettings.scala - настройки сборки: sbt-assembly, плагины компилятора
    - Dependencies.scala - зависимости проекта
- src
    - main
        - resources
            - tests - примеры тестов
        - scala
            - `com.tsc`
                - `common`
                    - `functions` - различные функции, используемые во всём проекте
                    - `logging` - классы, отвечающие за логгирование найденных различий при выполнении тестов
                    - `sparktest` - классы тестов и executor тестов
                        - `SparkTest` - класс теста, создается для каждой пары тестовых файлов source/target. Хранит всю
                          информацию для запуска теста.
                        - `TestExecutor` - парсит файлы тестов, группирует их и создает тесты из них, а затем запускает
                        - `TestResult` - результат теста
                - `exceptions` - обределения специфичных для проекта ошибок
                - `implicits` - capability extension классы
                - `validators` - функции для сравнения датасетов и выполнения проверок на них
            - `test` - объекты для запуска тестов
    - test
        - resources
            - sql_tests - файлы тестов, используются для проверки корректности работы классов из
              пакета `com.tsc.common.sparktest`
            - test_data - данные для тестирования корректности работы функций
        - scala - Unit-тесты, использована библиотека scalatest

## Формат и описание тестов

> :exclamation: Все файлы должны иметь расширение **.sql** и быть сохранены с кодировкой
> **UTF-8 без BOM** и с разделитетем строки - **LF**.

Название файла должно соответствовать шаблону:
> test_{number}\_{destination}\_{type}.sql

- number - числовой номер теста, максимум 3 цифры
- destination - source или target
- type - тип теста: arrays, counts, constants, ddl

Каждый тест должен состоять из двух файлов: source и target с одинаковым номером и типом.

Пример корректного именования тестовых файлов для теста на равенство данных (Arrays):
> test_1_source_arrays.sql
>
> test_1_target_arrays.sql

> :warning: **ORDER BY** следует использовать только если сортировка требуется в оконной функции.
> Использование лишней сортировки увеличит время выполнения тестов.

После завершения выполнения всех тестов, если все тесты были выполнены успешно, программа завершается с кодом 0. Если
хотя бы один тест был завершен с ошибкой или отрицательным результатом, а также если присутствуют некорректные тесты (
например, не хватает target-файла для какого-либо теста), то выбрасывается исключение, содержащее информацию о
результате выполнения тестов. При несоответствии данных в сравниваемых таблицах, в сообщение включаются различающиеся
строки. Максимальное количество строк задается в коде main-класса.

### Arrays

Описание: Проверка двух таблиц на равенство содержащихся в них данных. При сравнении не учитывается схема данных, тест
может завершится успешно, если типы данных различаются (например int и bigint, decimal(20,0) и decimal(30,0)), но
значения в таблицах совпадают.

**Source:** SQL-запрос выборки данных для сравнения
([Пример](src/main/resources/tests/dm_sum_operations/test_1_source_arrays.sql))

**Target:** SQL-запрос выборки данных для сравнения
([Пример](src/main/resources/tests/dm_sum_operations/test_1_target_arrays.sql))

### Counts

Описание: проверка таблиц на равенство количества содержащихся в них строк

**Source:** SQL-запрос на выборку данных для подсчета
([Пример](src/main/resources/tests/dm_sum_operations/test_1_source_counts.sql))

**Target:** SQL-запрос на выборку данных для подсчета
([Пример](src/main/resources/tests/dm_sum_operations/test_1_target_counts.sql))

### DDL

> :exclamation: для разделения названия поля и типа данных используется **точка с запятой(;)**. В конце строки её быть не должно.
> Source-файл **не чувствителен** к регистру.

Описание: проверка схемы данных

**Source:** описание схемы данных в формате:

> **_название_поля;тип_данных_**

([Пример source-файла для ddl теста](src/main/resources/tests/dm_sum_operations/test_1_source_ddl.sql))

**Target:** запрос describe к проверяемой таблице.
([Пример](src/main/resources/tests/dm_sum_operations/test_1_target_ddl.sql))

### Constants

> :exclamation: для разделения констант используется **точка с запятой(;)**.
> В конце строки её быть не должно.

Описание: проверка на равенство константным значениям.

**Source:** SQL-запрос на выборку данных.
([Пример](src/main/resources/tests/dm_sum_operations/test_1_source_constants.sql))

**Target:** Константы в формате:

> **_константа1;константа2;константаN_**

([Пример target-файла для constants теста](src/main/resources/tests/dm_sum_operations/test_1_target_constants.sql))

## Использование

### Запуск тестов, расположенных в папке resources

#### В одном потоке

Для запуска тестов нужно использовать [`com.tsc.run.Run`](src/main/scala/com/tsc/run/Run.scala), указав только
директории с тестами в `resources`, если тесты расположены не в `resources/tests` или не только в ней.

#### Параллельно
С аргументами по умолчению тесты выполняются по одному, есть возможность запустить тесты параллельно.
Для параллельного выполнения тестов нужно задать ExecutionContext в параметрах приложения.
([см. ниже](#параметры-командной-строки)).

#### Создание собственного класса/объекта

Можно создать собственный класс/объект, который будет наследовать
[`com.tsc.common.sparktest.TestExecutor`](src/main/scala/com/tsc/common/sparktest/TestExecutor.scala). Для запуска
тестов нужно вызвать метод `startAllTests`. Он принимает следующие аргументы:

- `throwError: Boolean` - выбросить ошибку после выполнения всех тестов если хотя бы один тест не был выполнен успешно.
  Если false, то просто логгирует результат.

implicit параметры:

- `spark: SparkSession` - SparkSession, которая будет выполнять тесты.
- `loggerFactory: ValidationLoggerFactory` - реализация трейта ValidationLoggerFactory.
  ([см. `com.tsc.common.logging`](src/main/scala/com/tsc/common/logging))
- `executionContext:  ExecutionContext` - ExecutionContext для запуска тестов.;

Пример реализации класса [`TestExecutor`](src/main/scala/com/tsc/common/sparktest/TestExecutor.scala):

> :warning: DataDifferenceLoggerFactory в примере ниже будет создавать сообщение, содержащее отличающиеся строки.  
```scala
package com.tsc.run

import com.tsc.common.logging.factories.{DataDifferenceLoggerFactory, ValidationLoggerFactory}
import com.tsc.common.sparktest.TestExecutor
import org.apache.spark.sql.SparkSession

import scala.concurrent.ExecutionContext

object CustomRun extends TestExecutor {

  implicit val spark: SparkSession = SparkSession
    .builder()
    .getOrCreate()

  val rowsToShowLimit = 20
  //Warning: Будет логгировать несовпавшие данные
  implicit val loggerFactory: ValidationLoggerFactory = DataDifferenceLoggerFactory(rowsToShowLimit)
  implicit val ec: ExecutionContext = config.executionContext

  startAllTests()
}

```

Для запуска в Airflow требуется указать созданный объект и параметры командной строки, описанные ниже.

### Параметры командной строки

> `-d, --dirs <dir1>,<dir2>...` - директории с тестами, расположенные в `resources`.
> Нужно указать относительный `resources` путь, по умолчанию тесты читаются из `tests/`
>
> `-e, --executionContext {cachedThreadPool | fixedThreadPool=n | workSteelingPool=n | singleThread}` - Используемый
> ExecutionContext, для fixedThreadPool и workSteelingPool требуется задать параллелизм. По умолчанию используется
> singleThread, то есть запуск осуществляется в одном потоке. ([Подробнее про Executors](https://docs.oracle.com/javase/7/docs/api/java/util/concurrent/Executors.html)).

### Использование функций для написания тестов на scala

Чтобы использовать функции для написания тестов или написания кода для автоматического запуска тестов, можно
воспользоваться функциями из пакета [`com.tsc.validators`](src/main/scala/com/tsc/validators/package.scala).

Можно также испортировать capability extension классы из `com.tsc.implicits`

```scala
import com.tsc.common.logging.loggers.ValidationLogger
import com.tsc.common.logging.loggers.{DebugValidationLogger, ValidationLogger}
import org.apache.spark.sql.{DataFrame, SparkSession}
import com.tsc.validators._

implicit val spark: SparkSession = ???
implicit val validationLogger: ValidationLogger = new DebugValidationLogger
val dataset1: DataFrame = ???
val dataset2: DataFrame = ???
val result1: Boolean = assertEquality(dataset1, dataset1)

//с использованием неявных преобразований

import com.tsc.implicits.DatasetTestOps

val result2: Boolean = dataset1.compareEqualityTo(dataset2)
//проверка, соответствуют ли все значения паттерну
val result3: Boolean = dataset1.assertPattern("Account No", "^\\d{1,12}'$")
```

## Пример запуска

Запуск тестов, расположенных в resources/datamart1 и resources/datamart2 с 5 потоками выполнения.

```shell
spark-submit --master yarn --class test.Run /
 path/to/sparktesting-x.y.z.jar -d tests/datamart1/ tests/datamart2/ -e fixedThreadPool=5
```
